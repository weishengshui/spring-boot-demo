package com.example.myproject.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import cn.wonhigh.o2o.center.organization.service.CompanyOrgCodeService;

import com.alibaba.dubbo.config.spring.ReferenceBean;

/**
 * dubbo引用服务设置。
 * <p>
 * 可以分模块写多个配置文件，如organization-center写一个、wechat-center写一个。
 * </p>
 * 
 * @author wei.ss
 * @date 2017年5月19日
 * @copyright wonhigh.cn
 */
@Configuration
public class DubboRefreenceConfig {

	@Bean(name = "companyOrgCodeService")
	public ReferenceBean<CompanyOrgCodeService> companyOrgCodeService() {
		ReferenceBean<CompanyOrgCodeService> ref = new ReferenceBean<CompanyOrgCodeService>();
		ref.setProxy("jdk"); // 不设置会报错，默认使用javassist做代理
		ref.setVersion("1.1");// 设置版本
		ref.setInterface(CompanyOrgCodeService.class);// 设置接口
		// bean.setId("companyOrgCodeService"); // 设置bean id，和方法名一样

		return ref;
	}
}
