package com.example.myproject.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.alibaba.dubbo.config.ApplicationConfig;
import com.alibaba.dubbo.config.ConsumerConfig;
import com.alibaba.dubbo.config.MonitorConfig;
import com.alibaba.dubbo.config.ReferenceConfig;
import com.alibaba.dubbo.config.RegistryConfig;

/**
 * dubbo基础配置，配置注册中心、协议、超时时间、应用名称、拥有人等
 * 
 * 
 * @author wei.ss
 * @date 2017年5月19日
 * @copyright wonhigh.cn
 */
@Configuration
public class DubboBaseConfig {

	// 注册中心地址
	@Value("${dubbo.zookeeper.registry.address}")
	private String registryAddress;
	// dubbo缓存文件路径
	@Value("${registry.cached.file}")
	private String cachedFile;

	/**
	 * 注册中心配置
	 * 
	 * @return
	 * @author wei.ss
	 */
	@Bean
	public RegistryConfig registryConfig() {
		RegistryConfig config = new RegistryConfig();
		config.setAddress(registryAddress);
		config.setFile(cachedFile);
		config.setProtocol("zookeeper");
		config.setClient("zkclient");

		return config;
	}

	/**
	 * 应用配置
	 * 
	 * @return
	 * @author wei.ss
	 */
	@Bean
	public ApplicationConfig applicationConfig() {
		ApplicationConfig config = new ApplicationConfig();
		config.setName("pointback-server");// 应用名字
		config.setOwner("wei.ss,li.xj"); // 拥有者

		return config;
	}

	/**
	 * 监控中心配置
	 * 
	 * @return
	 * @author wei.ss
	 */
	@Bean
	public MonitorConfig monitorConfig() {
		MonitorConfig monitorConfig = new MonitorConfig();
		monitorConfig.setProtocol("registry");

		return monitorConfig;
	}

	/**
	 * 消费者设置超时时间。消费者才需要设置 FIXME
	 * 
	 * @return
	 * @author wei.ss
	 */
	@Bean
	public ConsumerConfig consumerConfig() {
		ConsumerConfig consumerConfig = new ConsumerConfig();
		consumerConfig.setTimeout(30000);

		return consumerConfig;
	}
	
	/**
	 * 引用配置
	 * 
	 * @return
	 * @author wei.ss
	 */
	@Bean
	@SuppressWarnings("rawtypes")
	public ReferenceConfig referenceConfig(MonitorConfig monitorConfig) {
		ReferenceConfig rc = new ReferenceConfig();
		rc.setProxy("jdk"); // 不设置会报错，默认使用javassist做代理 
		rc.setMonitor(monitorConfig);
		
		return rc;
	}

	/**
	 * 提供者设置超时时间，负载均衡策略。只有生产者需要设置 FIXME
	 * 
	 * @return
	 * @author wei.ss
	 */
	// @Bean
	// public ProviderConfig providerConfig() {
	// ProviderConfig providerConfig = new ProviderConfig();
	// providerConfig.setTimeout(30000);
	// providerConfig.setLoadbalance("roundrobin");
	//
	// return providerConfig;
	// }

	/**
	 * 提供者设置发布服务的端口。只有生产者需要设置 FIXME
	 * 
	 * @return
	 * @author wei.ss
	 */
	// @Bean
	// public ProtocolConfig protocolConfig() {
	// ProtocolConfig protocolConfig = new ProtocolConfig();
	// protocolConfig.setPort(20880);
	//
	// return protocolConfig;
	// }
}
