package com.example.myproject.service.impl;

import org.springframework.stereotype.Service;

import com.example.myproject.service.HelloService;

/**
 * TODO: 增加描述
 * 
 * @author wei.ss
 * @date 2017年5月17日 
 * @copyright wonhigh.cn 
 */
@Service
public class HelloServiceImpl implements HelloService {
	
	@Override
	public String sayHello(String name) {
		return "你好，" + name;
	}
}
