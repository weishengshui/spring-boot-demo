package com.example.myproject.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import com.example.myproject.common.interceptors.AllInterceptor;
import com.example.myproject.common.interceptors.FileUploadInterceptor;

/**
 * web配置
 * 
 * @author wei.ss
 * @date 2017年5月18日
 * @copyright wonhigh.cn
 */
@Configuration
public class WebConfig extends WebMvcConfigurerAdapter {
	
	private static final Logger LOG = LoggerFactory.getLogger(WebConfig.class);
	
	/**
	 * 视图解析器
	 * 
	 * @return
	 * @author wei.ss
	 */
	@Bean
	public ViewResolver viewResolver() {
		
		LOG.info("ss");
		
		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
		viewResolver.setPrefix("/WEB-INF/jsp/");
		viewResolver.setSuffix(".jsp");

		return viewResolver;
	}

	/**
	 * 拦截器配置
	 */
	@Override
	public void addInterceptors(InterceptorRegistry registry) {

		registry.addInterceptor(new AllInterceptor());// 拦截所有
		registry.addInterceptor(new FileUploadInterceptor()).addPathPatterns(
				"/fileUpload.do");// 只拦截文件上传
	}

	// @Bean
	// public ServletRegistrationBean dispatcherRegistration(
	// DispatcherServlet dispatcherServlet) {
	// ServletRegistrationBean reg = new ServletRegistrationBean();
	// reg.setEnabled(true);
	// reg.setServlet(dispatcherServlet);
	// reg.addUrlMappings("*.html");
	// reg.addUrlMappings("*.do");
	// reg.setLoadOnStartup(1);
	//
	// return reg;
	// }

}
