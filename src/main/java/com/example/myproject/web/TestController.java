package com.example.myproject.web;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * TODO: 增加描述
 * 
 * @author wei.ss
 * @date 2017年5月18日 
 * @copyright wonhigh.cn 
 */
@Controller
public class TestController {
	
	private static final Logger LOG = LoggerFactory.getLogger(TestController.class);
	
	@RequestMapping(value="/test")
	public String test(){
		
		return "test";
	}
	
	@RequestMapping(value="/fu")
	public String fu(HttpServletRequest request){
		
		LOG.info(request.getServletPath());
		return "fileUpload";
	}
}
