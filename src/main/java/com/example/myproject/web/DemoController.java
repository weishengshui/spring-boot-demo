package com.example.myproject.web;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import cn.wonhigh.o2o.center.organization.service.CompanyOrgCodeService;

import com.example.myproject.service.HelloService;

/**
 * TODO: 增加描述
 * 
 * @author wei.ss
 * @date 2017年5月17日
 * @copyright wonhigh.cn
 */
@Controller
public class DemoController {

	private static final Logger LOG = LoggerFactory
			.getLogger(DemoController.class);

	@Autowired
	private HelloService helloService;
	@Resource(name = "companyOrgCodeService")
	private CompanyOrgCodeService companyOrgCodeService;

	@RequestMapping(value = "/", produces = { MediaType.TEXT_PLAIN_VALUE })
	@ResponseBody
	public String home() {
		LOG.info("log test companyOrgCodeService={}", companyOrgCodeService);

		return "Hello World2!中文测试" + "\n" + helloService.sayHello("水哥");
	}

	@ResponseBody
	@RequestMapping(value = "/cookieTest", produces = { MediaType.TEXT_PLAIN_VALUE })
	public String cookieTest(HttpServletResponse response) {
		
		setCookie("123", "12345", "localhost", response);
		
		// 跨域设置cookie无效
		setCookie("123", "12345", "dev.qxclub.cn", response);

		return "跨域设置cookie";
	}

	private void setCookie(String name, String value, String domain,
			HttpServletResponse response) {
		Cookie cookie = new Cookie(name, value);
		cookie.setDomain(domain);
		cookie.setPath("/");
		
		response.addCookie(cookie);
	}

	@RequestMapping(value = "/index")
	public ModelAndView index() {
		LOG.info("index companyOrgCodeService={}", companyOrgCodeService);

		String shopCode = "CA01MA";
		String brandCode = "MA";

		String orgCodeLong = companyOrgCodeService.queryOrgCodeLong(brandCode,
				shopCode, 6);
		LOG.info("orgCodeLong={}", orgCodeLong);

		ModelAndView mav = new ModelAndView("index");

		return mav;
	}

	/**
	 * 文件上传
	 * 
	 * @param file
	 * @return
	 * @author wei.ss
	 */
	@RequestMapping(value = "/fileUpload", method = RequestMethod.POST, produces = { MediaType.TEXT_PLAIN_VALUE })
	@ResponseBody
	public String fileUpload(@RequestParam("file1") MultipartFile file,
			String name) throws Exception {

		LOG.info("fileUpload file={}, name={}", file, name);
		if (null == file) {
			return "file not found";
		}

		name = file.getOriginalFilename();
		LOG.info("{}={}", name, file);

		if (file.getSize() == 0) {
			LOG.warn("文件没有内容：{}", name);
		}

		LOG.info("{} file's length is {}", name, file.getBytes().length);

		return "ok";
	}
}
