package com.example.myproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ImportResource;

import cn.wonhigh.o2o.center.organization.service.CompanyOrgCodeService;

@SpringBootApplication
@ImportResource(locations = { "classpath:/META-INF/spring/applicationContext.xml" })
public class Example {

	public static void main(String[] args) throws Exception {
		// SpringApplication application = new SpringApplicationBuilder().
		SpringApplication application = new SpringApplication(Example.class);
		application.setWebEnvironment(true);
		ConfigurableApplicationContext context = application.run(args);
		
		boolean result = context.containsBean("companyOrgCodeService");
		context.getBean(CompanyOrgCodeService.class);
		System.out.println(result);
		
	}

}